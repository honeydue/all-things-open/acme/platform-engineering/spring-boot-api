package cloud.honeydue.api.model;

import org.hibernate.annotations.ColumnTransformer;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name="users")
@Access(AccessType.FIELD)
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String nickname;
    private String emailAddress;
    private String lastName;
    private String firstName;
    @ColumnTransformer(read = "pgp_sym_decrypt(password, 'wandering-firefly-4506')", write = "pgp_sym_encrypt(?, 'wandering-firefly-4506')")
    private String password;
    @OneToMany(targetEntity = Chore.class, mappedBy = "user", fetch=FetchType.EAGER, cascade = CascadeType.ALL)
    private List<Chore> chores;

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getNickname() { return nickname; }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public List<Chore> getChores() {
        return chores;
    }

    public void setChores(List<Chore> chores) {
        this.chores = chores;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Id
    public Long getId() {
        return id;
    }


}
